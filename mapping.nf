nextflow.enable.dsl = 2

//include {sradownload} from "./sradownload"

params.reference = "./bat_genome_ref.fasta"
params.fastq_files = "./SRR1777174/SRR1777174_trim.fastq"

process bowtie2 {
  storeDir "${accession}"
  container "https://depot.galaxyproject.org/singularity/bowtie2%3A2.4.4--py39hbb4e92a_0"
  input:
    val accession
    path reference_fastq
    path fastq_files
    
  output:
    path "${reference_fastq.getSimpleName()}.sam"
  script:
    """
    bowtie2-build ${reference_fastq} ./index
    bowtie2 -x index -U ${fastq_files} -p ${task.cpus} -S ${fastq_files.getSimpleName()}.sam
    """

}


workflow {
    in_ref = channel.fromPath(params.reference)
    in_fastq = channel.fromPath(params.fastq_files)
    bowtie2(params.accession, in_ref, in_fastq)

}

/*
workflow bowtie_mapping {
    take:
      accession 
      reference
      fastq_files
      
    main:
      mapping_out = bowtie2(accession, reference, fastq_files)
      
    emit:
     mapping_out
}


workflow { 
  sra_download = sradownload(params.accession, params.with_fastp = true)
  sra_download.fastp_trimmed_fastq.view()
  test= bowtie_mapping(params.accession, params.reference, sra_download.fastp_trimmed_fastq)

}
*/
