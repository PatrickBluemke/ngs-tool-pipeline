nextflow.enable.dsl = 2

include {sradownload} from "./sradownload"
//include {quality_control} from "./quality_control"
//include {assembly} from "./de_novo_assembly"
//include {multiqc} from "./reporting"

params.with_fastqc = false
params.with_fastp = false
params.with_stats = false
params.with_velvet = false
params.with_spades = false
params.multiqc = false
params.kmerlength = 91
params.cov_cutoff = 20
params.min_contig_lgth = 200
params.quast = null

//accession number is also used as name for outdir

workflow {
    sequences = sradownload(params.accession, params.with_fastp)
    
    //sequences.fastp_trimmed_fastq.view()
    //sequences.fastq_files_raw.view()
    //sequences.fastp_json_files.view()
    
    //assembly(sequences.fastp_trimmed_fastq.collect())
}

    //vorbereitungresult = vorbereitung(params.accession, params.outdir, params.with_stats, params.with_fastqc, params.with_fastp)
    //assembly(vorbereitungresult.results_trimmed, params.outdir, params.kmerlength, params.cov_cutoff,params.min_contig_lgth, params.with_velvet, params.with_spades)
