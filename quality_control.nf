nextflow.enable.dsl = 2


process fastqc {
  storeDir "${params.outdir}/${params.accession}_stats" 
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input: 
    path fastq_file
  output:
    path "*fastqc.html", emit: html_files
    path "*fastqc.zip", emit: zip_files
  script:
    """
    fastqc ${fastq_file} 
    
    """
}


workflow quality_control {
    take:
      fastq_files 
      outdir
      with_fastqc
      
    main:
      
      params.outdir = outdir

      if (params.with_fastqc) {
          
          fastqc_output = fastqc(fastq_files.flatten())
          fastqc_zip_files = fastqc_output.zip_files
      } else {
        fastqc_zip_files = channel.empty()
      }
      
    emit:
     fastqc_zip_files
}


workflow { 

test= quality_control(fastqc_files)

test.fastqc_zip_files.view()
}


