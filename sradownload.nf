nextflow.enable.dsl = 2

params.with_fastp = false

process prefetch {
  storeDir "${accession}" 
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}.sra", emit: sra_file 
  script:
    """
    prefetch $accession
    mv ${accession}/${accession}.sra .
    """
}

process fastq_dump {
  storeDir "${accession}" 
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
    path sra_download
  output:
    path "${accession}.fastq", emit: fastq //für unpaired reads
  script:
    """
    fastq-dump --split-e $sra_download
    """
}

process fastp {
  storeDir "${accession}" 
  container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0"
  input:
    val accession
    path unprocessed_fastq
  output:
    path "*_trim.fastq", emit: fastq_trimmed
    path "*json", emit: fastp_json
    path "*html", emit: fastp_html
  script:
    """
    fastp -i ${unprocessed_fastq} -o ${unprocessed_fastq.getSimpleName()}_trim.fastq
    mv fastp.json ${unprocessed_fastq.getSimpleName()}_fastp.json
    mv fastp.html ${unprocessed_fastq.getSimpleName()}_fastp.html
    
    """
}



workflow sradownload {
    take:
      accession 
      //outdir
      with_fastp
      
    main:
      
      //params.accession = accession
      sra_download = prefetch(accession)
      fastq_files_raw = fastq_dump(accession, sra_download)
      
      if (params.with_fastp) {
          fastp_processed = fastp(accession, fastq_files_raw.flatten())
          fastp_trimmed_fastq = fastp_processed.fastq_trimmed
          fastp_json_files = fastp_processed.fastp_json
          
      } else {
        fastp_trimmed_fastq = channel.empty()
        fastp_json_files = channel.empty()
      }
      

    emit:
     fastq_files_raw
     fastp_trimmed_fastq
     fastp_json_files
}


workflow { 

test= sradownload(params.accession, params.with_fastp = false)
test.fastq_files_raw.view()
test.fastp_trimmed_fastq.view()
test.fastp_json_files.view()
}


